const elemento1 = document.getElementById("elemento1");
const elemento2 = document.getElementById("elemento2");
const elemento3 = document.getElementById("elemento3");
const elemento4 = document.getElementById("elemento4");

elemento1.addEventListener("mouseover", function() {
    this.style.borderColor = "#E6621F";
});

elemento1.addEventListener("mouseout", function() {
    this.style.borderColor = "";
});

elemento2.addEventListener("mouseover", function() {
    this.style.backgroundColor = "#E6621F";
    this.style.color = "#FFFFFF";
});

elemento2.addEventListener("mouseout", function() {
    this.style.backgroundColor = "";
    this.style.color = "";
});

elemento3.addEventListener("mouseover", function() {
    this.style.backgroundColor = "#E6621F";
    this.style.color = "#FFFFFF";
});

elemento3.addEventListener("mouseout", function() {
    this.style.backgroundColor = "";
    this.style.color = "";
});

elemento4.addEventListener("mouseover", function() {
    this.style.borderColor = "#E6621F";
});

elemento4.addEventListener("mouseout", function() {
    this.style.borderColor = "";
});